import time, threading
import sys
import math, random
import MySQLdb

from weight_simulator import WeightSimulator
from recorder import Recorder

import wiiboard_p3 as wiiboard
import pygame

import uuid

import RPi.GPIO as GPIO
GPIO_MODE_ALPHA = 23
GPIO_MODE_BETA = 24

time_window = 0.5 # seccond

def timekeep():
    time.sleep(time_window)

# matplotlib.use('tkagg')

#omoinoomoi@localhost
#omoinoomoi_Password_1

class Weighter(object):
    def __init__(self, *args, **kwargs):
        pass
    
    def main(self, ):
        board = wiiboard.Wiiboard()

        pygame.init()
        
        address = board.discover()
        board.connect(address) #The wii board must be in sync mode at this time

        time.sleep(0.1)
        board.setLight(True)
        done = False

        while not done:
            time.sleep(0.05)
            for event in pygame.event.get():
                # print("Event Got" + str(event))
                if event.type == wiiboard.WIIBOARD_MASS:
                    # print("--Mass event--   Total weight: " + str(event.mass.totalWeight) + ". Top left: " + str(event.mass.topLeft))			#etc for topRight, bottomRight, bottomLeft. buttonPressed and buttonReleased also available but easier to use in seperate event 
                    if (event.mass.totalWeight > 10):   #10KG. otherwise you would get alot of useless small events!
                        print(" over --Mass event--   Total weight: " + str(event.mass.totalWeight) + ". Top left: " + str(event.mass.topLeft))			#etc for topRight, bottomRight, bottomLeft. buttonPressed and buttonReleased also available but easier to use in seperate event

                elif event.type == wiiboard.WIIBOARD_BUTTON_PRESS:
                    print("Button pressed!")

                elif event.type == wiiboard.WIIBOARD_BUTTON_RELEASE:
                    print("Button released")
                    done = True

                #Other event types:
                #wiiboard.WIIBOARD_CONNECTED
                #wiiboard.WIIBOARD_DISCONNECTED

        board.disconnect()
        pygame.quit()

    def get_weight(self, ):
        pass

class Service(object):
    counter = 0
    modes = {'NON': 0, 'MESURE': 1, 'PLAY': 2}
    status = {'NON': 0, 'IN': 1, 'DONE': 2}
    stage = {'START':0, 'A': 1, 'B': 2, 'C': 3, 'OVER': 4, 'NONE': 5}
    CURRENT_STAGE = stage['START']
    STAGE_A = status['NON']
    STAGE_B = status['NON']
    STAGE_C = status['NON']

    mode = modes['MESURE']

    gpio_mode = 1

    th_a = 10 #kg
    th_b = 10 #kg 仮
    th_b_before = 10 #kg
    th_c = 10 #kg

    th_w = 0.5 #kg

    father_w = 0 #kg
    father_child_w = 0 #kg
    child_w = 0 #kg

    isFatherLeft = None
    isRecordSuccess = None
    isRecording = False
    isRecordEnded = False
    isProcessEnded = False
    isWeightEstimationEnd = False
    isMeasureEnd = False

    sample_window = 3 #samples #当初10でしたが、時間かかる。

    sample = []
    sample_average = 0 #kg
    sample_max = 0 #kg
    sample_min = 0 #kg

    filename = ""

    def __init__(self, *args, **kwargs):
        self.ws = WeightSimulator()
        self.funcs = [timekeep, self.ws.update,self.update_measurement]
        self.threads = []
        pass
    
    def start_switch_watch(self,):
        GPIO.setmode(GPIO.BCM)  # 初期設定
        # GPIO.setup(15,GPIO.OUT) # 出力設定
        GPIO.setup(GPIO_MODE_ALPHA,GPIO.IN)  # 入力設定
        GPIO.setup(GPIO_MODE_BETA,GPIO.IN)  # 入力設定

        try:
            # スイッチが入っているとする.
            self.gpio_mode = GPIO_MODE_ALPHA
            # タクトスイッチの状態をもとに判定する
            # 押す=HIGH はなす=LOW
            while True:
                if self.gpio_mode == GPIO_MODE_ALPHA:
                    print("> A | IN GPIO_MODE_ALPHA")
                    if self.mode is not modes['MESURE']:
                        self.mode = modes['MESURE']
                        print(">>>>>>>>>>>>>>> MESUER MODE <<<<<<<<<<<<<<<")
                        # Mesure Thread start
                elif self.gpio_mode == GPIO_MODE_BETA:
                    print("> B | IN GPIO_MODE_BETA")
                    if self.mode is not modes['PLAY']:
                        self.mode = modes['PLAY']
                        print(">>>>>>>>>>>>>>> PLAY MODE <<<<<<<<<<<<<<<")
                        # Play Thread start
                else:
                    #出来るだけクールダウンさせたい。
                    pass

                # if GPIO.input(GPIO_MODE_ALPHA) == GPIO.HIGH:
                #     print("> A | IN GPIO_MODE_ALPHA")
                # elif GPIO.input(GPIO_MODE_BETA) == GPIO.HIGH:
                #     print("> B | IN GPIO_MODE_BETA")
                # else:
                #     print("OFF")

                if self.isMeasureEnd:
                    self.isProcessEnded = True
                    break

                time.sleep(1)

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()


        GPIO.cleanup() # GPIO初期化

    def record_omoi(self,):
        self.isRecording = True
        print("======================== Record Start ========================")
        recorder = Recorder()
        recorder.record(self.filename)
        # time.sleep(10)
        self.isRecording = False
        self.isRecordEnded = True
        self.isRecordSuccess = True
        print("======================== Record END ========================")

    def _proc_stop(self, parameter_list):
        pass

    def _proc_pend(self, parameter_list):
        pass

    def _c_proc_push_to_local_database(self, timestamp, child_weight, parent_weight, record_name):
        con = MySQLdb.connect(
            user='omoinoomoi',
            passwd='omoinoomoi_Password_1',
            host='127.0.0.1',
            db='omoi_database'
        )
        cur = con.cursor()

        # sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values('"+ str(timestamp) +"',"+ str(child_weight) +","+ str(parent_weight) +",'"+ str(record_name) +"')"
        sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
        cur.execute(sql, (timestamp,child_weight,parent_weight,record_name))

        # rows = cur.fetchall()
        # for row in rows:
        #     print(row)
        con.commit()

        cur.close
        con.close

    def _c_proc_push_to_cloud_database(self, ):
        pass

    def _proc_push_to_database(self, ):
        print('Pushing to database')
        timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
        child_weight = float(self.child_w)
        parent_weight = float(self.father_w)
        record_name = self.filename
        # we should use uuid. https://ja.wikipedia.org/wiki/UUID  https://www.python.ambitious-engineer.com/archives/1436
        # record_name = str(timestamp) + "_" + str(self.child_w) + "_" + str(self.father_w)
        self._c_proc_push_to_local_database(timestamp, child_weight, parent_weight,record_name)

    # EVALUATORS
    def _proc_stage_START_evaluate_stage(self, weight):
        eval = False
        if weight > self.th_a:
            eval = True
        return eval

    def _proc_stage_A_evaluate_stage(self, weight):
        eval = False

        print("| | | -> [service] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        # Sample Management
        if len(self.sample) < self.sample_window: #3こ超えたら
            self.sample.append(weight) # リストの後ろにサンプルを追加
        elif len(self.sample) >= self.sample_window:
            self.sample.pop(0)  # リストの頭１つを削除
            self.sample.append(weight) # リストの後ろにサンプルを追加

        # Evaluation
        if len(self.sample) >= self.sample_window:
            self.sample_average = sum(self.sample)/len(self.sample)
            self.sample_max = max(self.sample)
            self.sample_min = min(self.sample)

            if abs(self.sample_max - self.sample_average) < self.th_w and abs(self.sample_min - self.sample_average) < self.th_w:
                self.father_w = self.sample_average
                self.sample.clear()
                self.th_b = self.father_w + 1 #1kg上を設定する。
                eval = True

        return eval

    def _proc_stage_B_evaluate_stage(self, weight):
        eval = False
        if weight < self.th_b_before:
            self.isFatherLeft = True
        if self.isFatherLeft and weight > self.th_b:
            eval = True
        return eval

    def _proc_stage_C_evaluate_stage(self, weight):
        eval = False

        print("| | | | -> [service] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        # Sample Management // 関数として切り離すべき
        if len(self.sample) < self.sample_window: #3こ超えたら
            self.sample.append(weight) # リストの後ろにサンプルを追加
        elif len(self.sample) >= self.sample_window:
            self.sample.pop(0)  # リストの頭１つを削除
            self.sample.append(weight) # リストの後ろにサンプルを追加

        # Evaluation // 関数として切り離すべき
        if len(self.sample) >= self.sample_window:
            self.sample_average = sum(self.sample)/len(self.sample)
            self.sample_max = max(self.sample)
            self.sample_min = min(self.sample)

            if abs(self.sample_max - self.sample_average) < self.th_w and abs(self.sample_min - self.sample_average) < self.th_w:
                self.father_child_w = self.sample_average
                self.sample.clear()
                self.child_w = self.father_child_w - self.father_w
                eval = True

        return eval

    def _proc_stage_OVER_evaluate_stage(self, weight):
        eval = False
        # if weight < self.th_c:
        #     eval = True
        if self.isWeightEstimationEnd is True and (self.isRecordSuccess or self.isRecordEnded) is True:
            self._proc_push_to_database() # 例外処理を
            print("=======================================")
            print("Ending Program. |Result| Father Weight:" + str(self.father_w) +" Child Weight:" + str(self.child_w) + " Record Successed." )
            print("=======================================")
            self.counter += 1 #動作記録用

            self.isMeasureEnd = True
            eval = True
        return eval

    def controller(self, weight):
        print('[Controller] Stage Code:'+ str(self.CURRENT_STAGE) )# + 'Stage A Status Code A:'+ str(self.STAGE_A) + ' B:' + str(self.STAGE_B) + ' C:' + str(self.STAGE_C))
        if self.CURRENT_STAGE is self.stage['START']:
            if self.isRecording:
                print("=== Recording ===")
            elif self._proc_stage_START_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['A']

                self.isFatherLeft = False
                self.isRecordSuccess = False
                self.isMeasureEnd = False

                print('Movint to Stage A')
                # 音声再生すべき。
        elif self.CURRENT_STAGE is self.stage['A']:
            if self._proc_stage_A_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['B']
                print('Movint to Stage B')
                # 音声再生すべき。
        elif self.CURRENT_STAGE is self.stage['B']:
            if self._proc_stage_B_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['C']

                self.filename = str(uuid.uuid1())
                t = threading.Thread(target=self.record_omoi)
                t.setDaemon(True)
                t.start()

                print('Movint to Stage C')
                # 音声再生すべき。
        elif self.CURRENT_STAGE is self.stage['C']:
            if self._proc_stage_C_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['OVER']

                self.isWeightEstimationEnd = True

                print('Movint to Stage OVER')
        elif self.CURRENT_STAGE is self.stage['OVER']:
            if self._proc_stage_OVER_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['START']
                print(" <<< Getting Back to the START Stage.")
                # または、ここで終わるべき。
        else:
            print('[WARNINNG] Controller is something wrong.')
        if self.isRecording:
            print("=== Recording ===")

    def _event_weight_simulation_event(self, ):
        weight = self.ws.get_current_w()
        self.controller(weight)

    def _event_weight_event(self, ): #イベントごとに呼び出す
        weight =  10.0#self.ws.get_current_w()
        self.controller(weight)

    def update_measurement(self, ):
        print("============================================================")
        print('| Service update' + ' -> Current stage :'+ str(self.CURRENT_STAGE))
        print("| | -> [service] weight: " + str(self.ws.get_current_w()))
        self._event_weight_simulation_event()
    
    def update_play(self,):
        pass
    
    def start_real(self, ):
        # スイッチ確認
        # モードに応じて、スイッチが押されたらプロセス開始
        # モードに応じて、スイッチが押されたらプロセス開始
        pass
    
    def _proc_start_measure(self,):
        pass
    
    def _proc_start_play(self,):
        pass

    def start_simulator(self, ):
        try:
            #ここにforで、テスト回数を増やす
            while True:
                for func in self.funcs:
                    t = threading.Thread(target=func)
                    t.setDaemon(True)
                    t.start()
                    self.threads.append(t)
                    for t in self.threads:
                        t.join()
                if self.isMeasureEnd:
                    self.isProcessEnded = True
                    break

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

    def proc(self, ):
        # print("proc")
        # t = threading.Thread(target=test1)
        # t.setDaemon(True)
        # t.start()

        # try:
        #     t = threading.Thread(target=test1)
        #     t.setDaemon(True)
        #     t.start()
        #     # tt = threading.Thread(target=func)
        #     # tt.setDaemon(True)
        #     # tt.start()
        #     self.threads.append(t)
        #     # self.threads.append(tt)
        #     # for t in self.threads:
        #     #     t.join()
        # except KeyboardInterrupt:
        #     print('\n Keyborad Interrupted! Aborting.')
        #     pass
        #     sys.exit()
        pass

# def insert_test():
#     con = MySQLdb.connect(
#         user='omoinoomoi',
#         passwd='omoinoomoi_Password_1',
#         host='127.0.0.1',
#         db='omoi_database'
#     )
#     cur = con.cursor()

#     timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
#     child_weight = 10.0
#     parent_weight = 12.0
#     record_name = "test"

#     # sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values('"+ str(timestamp) +"',"+ str(child_weight) +","+ str(parent_weight) +",'"+ str(record_name) +"')"
#     sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
#     r = cur.execute(sql, (str(timestamp),str(child_weight),str(parent_weight),record_name))
#     print(r)
#     con.commit()

#     cur.close
#     con.close

if __name__ == "__main__":
    # insert_test()

    # s = Service()
    # s.start_simulator()

    # 毎回電源が入ったらこれ。
    s = Service()
    s.start_switch_watch()

    # w = Weighter()
    # w.main()

    # ==================================

    # ws = WeightSimulator()
    # ws.start()
    # ws.simulation_test()

    # v = View()
    # v.start()
    # v.seflupdate()

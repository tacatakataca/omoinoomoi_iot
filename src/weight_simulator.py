import math, random
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import time, threading
import sys

START = 0
PARENTON = 1
PARENTNCHILDON = 2
EXIT = 99
END = 3

time_window = 0.5 # seccond

target_weight = 68

def f(x, target):
    return 2*5*x**4 -2 + target

def monte(N, target):
    a = 0
    b = 1

    x = (b-a)*np.random.rand(N)
    ans = np.average(f(x,target))
    
    return ans

def timekeep():
    time.sleep(time_window)

class View(object):
    def __init__(self, *args, **kwargs):
        self.start()
    
    def start(self, ):
        self.times = [0 for i in range(100)]
        self.xs = [0 for i in range(100)]

        self.time = 0
        self.x = 0

        # initialize matplotlib
        plt.ion()
        plt.figure()
        self.li, = plt.plot(self.times, self.xs)

        plt.ylim(-1,100)
        plt.xlabel("time")
        plt.ylabel("weight")
        plt.title("weight real time plot")

        plt.xlim(min(self.times), max(self.times))
        plt.draw()
        plt.pause(0.01)

    def update(self, _x, _time):
        self.x = _x
        self.time = _time

        self.times.append(self.time)
        self.times.pop(0)
        self.xs.append(self.x)
        self.xs.pop(0)

        self.li.set_xdata(self.times)
        self.li.set_ydata(self.xs)

        plt.xlim(min(self.times), max(self.times))
        plt.draw()
        # plt.pause(0.01)

    def seflupdate(self, ):
        count = 0
        while True:
            self.update(math.sin(count), count)
            plt.pause(0.01)
            count += 1

class WeightSimulator():
    startx = 1
    endx = 5000
    interval = 1
    monte_count = 0
    current_w = 0
    time = 0

    total_weight = 0

    current_total_weight = 0
    current_child_weight = 0
    current_time = 0

    standing_stage = START

    def __init__(self, *args, **kwargs):
        self.father_weight = random.uniform(50,70)
        self.child_weight = random.uniform(2,20)
        self.father_standing_time = random.randint(20,30)
        self.child_standing_time = random.randint(20,30)
        self.exitting_time = random.randint(5,20)
        self.funcs = [timekeep, self.update]
        self.threads = []
        self.p_event = threading.Event()
        self.c_event = threading.Event()
        self.end_event = threading.Event()
        self.current_time = time.time()
        self.standing_stage = START
        self.view = View()

    def simulation_test(self, ):
        sample = np.arange(self.startx, self.endx+self.startx,self.interval)
        m_int = pd.DataFrame((np.random.randn(sample.size, 2)+2)*0)
        count = 0

        for i in sample:
            m_int[0][count] = i
            m_int[1][count] = monte(i)
            count += 1
        
        m_int = m_int.rename( columns={0:'N', 1:'ans'} )

        m_int.plot( x='N', figsize=(13,8), title="y=5x^4" )

        plt.grid(which='major', color='black', linestyle="-")
        plt.show()
    
    def get_nothing_weight(self, ):
        return 0
    
    def get_parent_simulated_weight(self, time):
        return monte(time, self.father_weight)
    
    def get_parent_child_simulated_weight(self, time):
        return monte(time, self.father_weight + self.child_weight)

    def onlyparent_standing(self, ):
        self.monte_count = 0
        self.situation = 'monte'
        self.standing_stage = PARENTON

    def childandparent_standing(self, ):
        self.monte_count = 0
        self.situation = 'monte'
        self.standing_stage = PARENTNCHILDON

    def exitting(self, ):
        self.situation = 'exitting'
        self.standing_stage = EXIT
    
    def get_current_w(self,):
        return self.current_w

    def get_weight(self, ):
        result = 0
        if self.standing_stage is EXIT or self.standing_stage is START:
            self.current_w = 0
            result = self.current_w
        if self.standing_stage is PARENTON:
            if self.situation is 'monte':
                self.current_w = self.get_parent_simulated_weight(self.time)
                result = self.current_w
            else:
                result = self.current_w
        elif self.standing_stage is PARENTNCHILDON:
            if self.situation is 'monte':
                self.current_w = self.get_parent_child_simulated_weight(self.time)
                result = self.current_w
            else:
                result = self.current_w
        else:
            self.current_w = 0
            result = 0
        return result

    def time_update(self, ):
        self.time += 1

    def check_simulation(self, ):
        if self.monte_count > self.endx:
            self.situation = 'non-monte'
        else:
            self.monte_count += 1
        if self.time is 5*2:
            self.onlyparent_standing()
        if self.time is (5*2 + self.father_standing_time):
            self.exitting()
        if self.time is (5*2 + self.father_standing_time + self.exitting_time):
            self.childandparent_standing()
        if self.time is (5*2 + self.father_standing_time + self.exitting_time + self.child_standing_time):
            self.exitting()

    def update(self, ):
        print('+ weight simulator update')
        self.check_simulation()
        if self.time > 0:
            self.view.update(self.get_weight(),self.time)
        print("+ + > wreight simulator weight: " + str(self.get_weight()))
        self.time_update()

    def start(self, ):
        try:
            t = threading.Thread(target=self.proc)
            t.setDaemon(True)
            t.start()

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

        while True:
            c = sys.stdin.read(1)
            if c == 'p':
                self.onlyparent_standing()
                print("parent standing")
            if c == 'c':
                self.childandparent_standing()
                print("standing with child")
            if c == ' ':
                print("Aborting.")
                pass
                sys.exit()
            if c == 'e':
                self.exitting()
                print("someone exited.")

    def proc(self, ):
        try:
            while True:
                for func in self.funcs:
                    t = threading.Thread(target=func)
                    t.setDaemon(True)
                    t.start()
                    self.threads.append(t)

                for t in self.threads:
                    t.join()
                print(str(self.time) + '> time:' + str(time.time() - self.current_time))
                # self.view.plt_update()
        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

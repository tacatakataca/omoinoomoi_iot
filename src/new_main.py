import time, threading
import sys
import subprocess
import math, random
import pygame.mixer

import MySQLdb

from weight_simulator import WeightSimulator
from recorder import Recorder

import wiiboard_p3 as wiiboard
import pygame
# from view import View

import uuid

import RPi.GPIO as GPIO
GPIO_MODE_ALPHA = 23
GPIO_MODE_BETA = 24

time_window = 0.2 # seccond

stop_event_wiiboard = threading.Event() #停止させるかのフラグ

def timekeep():
    time.sleep(time_window)

# matplotlib.use('tkagg')

#omoinoomoi@localhost
#omoinoomoi_Password_1

user_name = 'omoinoomosa'
passwd = 'omoinoomosa'
host='10.10.201.151',
db = 'omoinoomosa'


class Weighter(object):

    current_w = 0.0
    connected = False
    disconnected = True
    isView = True

    def __init__(self, *args, **kwargs):
        pass
        # self.stop_event_wiiboard = threading.Event() #停止させるかのフラグ

    def beep_start_measureing(self,):
        pygame.mixer.music.load("../asset/three.wav")
        pygame.mixer.music.play(1)
        time.sleep(1)
        pygame.mixer.music.stop()

    def get_current_w(self):
        return self.current_w
    
    def main(self, ):
        board = wiiboard.Wiiboard()

        pygame.init()
        pygame.mixer.init()

        address = board.discover()
        board.connect(address) #The wii board must be in sync mode at this time

        time.sleep(0.1)

        self.connected = board.isConnected()
        print(" >>>> connection : "+ str(self.connected))
        self.beep_start_measureing()

        if self.connected is False:
            return

        board.setLight(True)
        done = False

        while (not done) and not stop_event_wiiboard.is_set() :# and self.stop_event_wiiboard.is_set():
            time.sleep(0.05)
            for event in pygame.event.get():
                # print("Event Got" + str(event))
                if event.type == wiiboard.WIIBOARD_CONNECTED:
                    self.connected = True
                    self.disconnected = False
                if event.type == wiiboard.WIIBOARD_DISCONNECTED:
                    print("main disconected")
                    self.disconnected = True
                    self.connected = False
                if event.type == wiiboard.WIIBOARD_MASS:
                    # # print("--Mass event--   Total weight: " + str(event.mass.totalWeight) + ". Top left: " + str(event.mass.topLeft))			#etc for topRight, bottomRight, bottomLeft. buttonPressed and buttonReleased also available but easier to use in seperate event 
                    # if (event.mass.totalWeight > 10):   #10KG. otherwise you would get alot of useless small events!
                    #     # print(" over --Mass event--   Total weight: " + str(event.mass.totalWeight) + ". Top left: " + str(event.mass.topLeft))			#etc for topRight, bottomRight, bottomLeft. buttonPressed and buttonReleased also available but easier to use in seperate event
                    #     self.current_w = event.mass.totalWeight
                    # print("WEIGHT:"+str(event.mass.totalWeight))
                    self.current_w = (event.mass.totalWeight)

                elif event.type == wiiboard.WIIBOARD_BUTTON_PRESS:
                    print("Button pressed!")

                elif event.type == wiiboard.WIIBOARD_BUTTON_RELEASE:
                    print("Button released")
                    done = True

                #Other event types:
                #wiiboard.WIIBOARD_CONNECTED
                #wiiboard.WIIBOARD_DISCONNECTED

        board.disconnect()
        pygame.quit()

    def get_weight(self, ):
        pass

class Service(object):
    counter = 0

    time = 0

    modes = {'OFF': 0, 'MEASURE': 1, 'PLAY': 2}
    status = {'NON': 0, 'IN': 1, 'DONE': 2}

    stage = {'START':1, 'A': 2, 'B': 88, 'C': 4, 'OVER': 99, 'NONE': 6}
    play_stage = {'MEASURE_IN_PLAY':1, 'PLAY': 2, 'PLAY_OVER': 3, 'NONE': 4}

    CURRENT_STAGE = stage['START']
    PLAY_CURRENT_STAGE = play_stage['MEASURE_IN_PLAY']

    STAGE_A = status['NON']
    STAGE_B = status['NON']
    STAGE_C = status['NON']

    mode = modes['OFF']

    gpio_mode = 1

    th_a = 20 #kg
    th_b = 10 #kg 仮
    th_b_before = 10 #kg
    th_c = 10 #kg

    th_w = 0.5 #kg

    father_w = 0 #kg
    father_child_w = 0 #kg
    child_w = 0 #kg

    target_real_object_w = 0

    isSimulation = False

    # メインプロセスでしか使えねえ...
    isView = False

    isFatherLeft = None
    isRecordSuccess = None
    isRecording = False
    isRecordEnded = False
    isMeasureProcessEnded = False
    isWeightEstimationEnd = False
    isMeasureEnd = False

    sample_window = 30 #samples #当初10で。

    sample = []
    sample_average = 0 #kg
    sample_max = 0 #kg
    sample_min = 0 #kg

    filename = ""

    measure_process_end = False
    play_process_end = False


    # ===============================

    p_th_a = 1

    # ===============================
    def __init__(self, _isSimulation = False):
        self.isSimulation = _isSimulation
        if self.isSimulation:
            self.ws = WeightSimulator()
            self.funcs = [timekeep, self.ws.update,self.update_measurement]
        else:
            self.ws = ""
            self.funcs = [timekeep]
        self.weighter = Weighter()
        self.funcs_real = [timekeep, self.update_measurement]
        self.funcs_p = [timekeep,self.update_play]
        self.threads = []
        self.end_alart = False
        pygame.mixer.init()
    
    def beep_onece(self,):
        pygame.mixer.music.load("../asset/one.wav")
        pygame.mixer.music.play(1)
        time.sleep(1)
        pygame.mixer.music.stop()

    def beep_twice(self,):
        pygame.mixer.music.load("../asset/two.wav")
        pygame.mixer.music.play(1)
        time.sleep(1)
        pygame.mixer.music.stop()

    def beep_thrids(self,):
        pygame.mixer.music.load("../asset/three.wav")
        pygame.mixer.music.play(1)
        time.sleep(1)
        pygame.mixer.music.stop()

    def play_sound_of_file(self, file):
        filename = "../data/"+file+".wav"
        print("Playing sound file of :"+str(filename))
        # pygame.mixer.music.load(filename)
        # pygame.mixer.music.play(1)
        # time.sleep(1)
        # pygame.mixer.music.stop()
        subprocess.call("aplay "+filename, shell=True)
        return True

    def beep_wiiboard_start_connection(self,):
        self.beep_thrids()

    def beep_you_should_try_bluetooth_connection(self,):
        self.beep_onece()

    def beep_start(self,):
        self.beep_onece()

    def beep_end(self,):
        self.beep_onece()

    def beep_start_play(self,):
        self.beep_twice()

    def beep_start_measureing(self,):
        self.beep_twice()

    def beep_start_measureing_2A(self,):
        # self.beep_onece()
        pass

    def beep_start_measureing_2B(self,):
        self.beep_twice()

    def beep_start_measureing_2C(self,):
        # self.beep_twice()

        # self.beep_onece()
        pass

    def beep_start_measureing_OVER(self,):
        self.beep_twice()

    def beep_record_start(self,):
        self.beep_thrids()

    def beep_record_over(self,):
        self.beep_thrids()

    def beep_measured(self,):
        self.beep_thrids()

    def beep_play_end(self,):
        self.beep_thrids()

    def beep_test(self,):
        self.beep_onece()

    # def start(self, ):
    #     for func in self.funcs:
    #         t = threading.Thread(target=func)
    #         t.setDaemon(True)
    #         t.start()
    #         self.threads.append(t)
    #         for t in self.threads:
    #             t.join()

    # def gpoiTest(self, ):
    #     GPIO.setmode(GPIO.BCM)  # 初期設定
    #     # GPIO.setup(15,GPIO.OUT) # 出力設定
    #     GPIO.setup(GPIO_MODE_ALPHA, GPIO.IN)  # 入力設定
    #     GPIO.setup(GPIO_MODE_BETA, GPIO.IN)  # 入力設定

    #     while True:
    #         if GPIO.input(GPIO_MODE_ALPHA) is GPIO.HIGH and GPIO.input(GPIO_MODE_BETA) is GPIO.HIGH:
    #             print("OFF")
    #             self.beep_end()
    #         elif GPIO.input(GPIO_MODE_ALPHA) is GPIO.HIGH:
    #             self.beep_start_measureing()
    #             print("> A | IN GPIO_MODE_ALPHA")
    #         elif GPIO.input(GPIO_MODE_BETA) is GPIO.HIGH:
    #             self.beep_start_play()
    #             print("> B | IN GPIO_MODE_BETA")
    #         else:
    #             print("GPIO switch somthing is wrong.")
    #         time.sleep(1)

    def weight_thread_start(self, ):
        self.th_weighter = threading.Thread(name ='wiiboard',target=self.weighter.main)
        self.th_weighter.setDaemon(True)
        self.th_weighter.start()
        self.beep_wiiboard_start_connection()

    def get_thread_names(self,):
        list = []
        for t in threading.enumerate():
            # print("name :"+str(t.name))
            list.append(str(t.name))
        return list

    def start_switch_watch(self,):
        self.beep_start()
        GPIO.setmode(GPIO.BCM)  # 初期設定
        # GPIO.setup(15,GPIO.OUT) # 出力設定
        GPIO.setup(GPIO_MODE_ALPHA ,GPIO.IN)  # 入力設定
        GPIO.setup(GPIO_MODE_BETA ,GPIO.IN)  # 入力設定

        # wiiboard プロセス開始。
        self.weight_thread_start()

        self.stop_event_measure = threading.Event() #停止させるかのフラグ
        self.stop_event_play = threading.Event() #停止させるかのフラグ
        # self.inc_event = threading.Event()  #刻み幅を増やすかのフラグ

        # それぞれのプロセスを準備。


        # wiiboard 接続まで 10秒まつ
        time.sleep(10)

        # # 接続できなかったら、やめる。または、wiiboardを探すプロセスを数秒おきにループさせる。
        # while not self.weighter.connected:
        #     # self.weighter.stop_event_wiiboard.set()
        #     self.th_weighter.join()
        #     time.sleep(1)
        #     self.th_weighter = threading.Thread(target=self.weighter.main)
        #     self.th_weighter.setDaemon(True)
        #     self.th_weighter.start()
        #     print("connection : "+ str(self.weighter.connected))
        #     time.sleep(10)

        print("[OMOINOOMODS] STARTS")
        try:
            # スイッチが入っているとする.
            # self.gpio_mode = GPIO_MODE_ALPHA
            # タクトスイッチの状態をもとに判定する
            # 押す=HIGH はなす=LOW
            while True:
                print("==== Thread Names : "+str(self.get_thread_names()) +"=====")
                if GPIO.input(GPIO_MODE_ALPHA) is GPIO.HIGH and GPIO.input(GPIO_MODE_BETA) is GPIO.HIGH:
                # if self.gpio_mode == OFF:
                    print("OFF")
                    if self.mode is self.modes['MEASURE']:
                        if 'measuer' in self.get_thread_names():
                            self._proc_stop_measure()
                        # if 'wiiboard' in self.get_thread_names():
                        #     self._proc_stop_wiiboard()
                        self.mode = self.modes['OFF']

                    if self.mode is self.modes['PLAY']:
                        if 'play' in self.get_thread_names():
                            self._proc_stop_play()
                        if not 'wiiboard' in self.get_thread_names():
                            self.weight_thread_start()
                        # if 'wiiboard' in self.get_thread_names():
                        #     self._proc_stop_wiiboard()
                        self.mode = self.modes['OFF']

                    if self.mode is self.modes['OFF']:
                        if not ('wiiboard' in self.get_thread_names()):
                            self.weight_thread_start()
                        if not self.end_alart:
                            self.end_alart = True
                            self.beep_end()

                # elif self.gpio_mode == GPIO_MODE_ALPHA:
                # if GPIO.input(GPIO_MODE_ALPHA) == GPIO.HIGH:
                elif GPIO.input(GPIO_MODE_ALPHA) is GPIO.HIGH:
                    print("||| SWITCH [MEASURE MODE]")
                    if self.measure_process_end and 'measure' in self.get_thread_names():
                        self.measure_process_end = False
                        self._proc_stop_measure()
                    if self.mode is not self.modes['MEASURE']:
                        self.beep_start_measureing()
                        self.end_alart = False
                        self.mode = self.modes['MEASURE']
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>> MESUER MODE - PROCESS START <<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        # 状態遷移図的にありえないが...
                        if 'play' in self.get_thread_names():
                            self._proc_stop_play()
                        # Measure Thread start
                        self.th_measure = threading.Thread(name = "measuer",target=self._proc_start_measure)
                        self.th_measure.setDaemon(True)
                        self.th_measure.start()
                        print("TH M"+str(self.th_measure))
                # elif self.gpio_mode == GPIO_MODE_BETA:
                # elif GPIO.input(GPIO_MODE_BETA) == GPIO.HIGH:
                elif GPIO.input(GPIO_MODE_BETA) is GPIO.HIGH:
                    print("||| SWITCH [PLAY MODE]")
                    self.end_alart = False
                    if self.play_process_end and 'play' in self.get_thread_names():
                        self.play_process_end = False
                        self._proc_stop_play()

                    if self.mode is not self.modes['PLAY']:
                        self.beep_start_play()
                        self.mode = self.modes['PLAY']
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>> PLAY MODE - PROCESS START <<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        print(">>>>>>>>>>>>>>>.............................<<<<<<<<<<<<<<<")
                        # 状態遷移図的にありえないが...
                        if 'measuer' in self.get_thread_names():
                            self._proc_stop_measure()
                            # MEASURE プロセスで、レコードしてたりデータベースアクセスしてたら待って、殺すシグナル出したり、フラグを立てて殺す。
                        # Play Thread start
                        self.th_play = threading.Thread(name = "play",target=self._proc_start_play)
                        self.th_play.setDaemon(True)
                        self.th_play.start()
                else:
                    print("[Switch Conditionning] something is wrong.")
                    time.sleep(100)

                time.sleep(1)

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()


        GPIO.cleanup() # GPIO初期化

    def record_omoi(self,):
        self.beep_record_start()
        self.isRecording = True
        print("======================== Record Start ========================")
        recorder = Recorder()
        recorder.record(self.filename)
        # time.sleep(10)
        self.isRecording = False
        self.isRecordEnded = True
        self.isRecordSuccess = True
        print("======================== Record END ========================")

    def _proc_stop(self, parameter_list):
        pass

    def _proc_pend(self, parameter_list):
        pass

    def _c_proc_push_to_local_database(self, timestamp, child_weight, parent_weight, record_name):
        # con = MySQLdb.connect(
        #     user='omoinoomoi',
        #     passwd='omoinoomoi_Password_1',
        #     host='127.0.0.1',
        #     db='omoi_database'
        # )
        con = MySQLdb.connect(
            user='omoinoomosa',
            passwd='omoinoomosa',
            host='10.10.201.151',
            db='omoinoomosa'
        )
        cur = con.cursor()

        # sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
        sql = "insert into omoinoomosa.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
        cur.execute(sql, (timestamp,child_weight,parent_weight,record_name))

        # rows = cur.fetchall()
        # for row in rows:
        #     print(row)
        con.commit()

        cur.close
        con.close

    def _c_proc_push_to_cloud_database(self, ):
        pass

    def _proc_push_to_database(self, ):
        print('Pushing to database')
        timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
        child_weight = float(self.child_w)
        parent_weight = float(self.father_w)
        record_name = self.filename
        print("||| DATA ||| >>> " + str(timestamp)+ " : " + str(child_weight) + " : " + str(parent_weight) + " : " + record_name)
        # we should use uuid. https://ja.wikipedia.org/wiki/UUID  https://www.python.ambitious-engineer.com/archives/1436
        # 下の関数は、通常呼びます。
        self._c_proc_push_to_local_database(timestamp, child_weight, parent_weight,record_name)

    # EVALUATORS ==============================================================
    # EVALUATORS ==============================================================
    # EVALUATORS ==============================================================
    def _proc_stage_START_evaluate_stage(self, weight):
        eval = False
        # Sample Management
        if 1.0 < weight :
            if len(self.sample) < self.sample_window:
                self.sample.append(weight) # リストの後ろにサンプルを追加
            elif len(self.sample) >= self.sample_window:
                self.sample.pop(0)  # リストの頭１つを削除
                self.sample.append(weight) # リストの後ろにサンプルを追加
                    # Evaluation
            self.sample_average = sum(self.sample)/len(self.sample)

            if self.sample_average > self.th_a:
                self.sample_average = 0.0
                self.sample.clear()
                eval = True
        else:
            self.sample.clear()

        return eval

    def _proc_stage_A_evaluate_stage(self, weight):
        eval = False

        print("| | | -> [service] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        # # Sample Management
        # if len(self.sample) < self.sample_window: #3こ超えたら
        #     self.sample.append(weight) # リストの後ろにサンプルを追加
        # elif len(self.sample) >= self.sample_window:
        #     self.sample.pop(0)  # リストの頭１つを削除
        #     self.sample.append(weight) # リストの後ろにサンプルを追加

        if 1.0 < weight :
            if len(self.sample) < self.sample_window:
                self.sample.append(weight) # リストの後ろにサンプルを追加
            elif len(self.sample) >= self.sample_window:
                self.sample.pop(0)  # リストの頭１つを削除
                self.sample.append(weight) # リストの後ろにサンプルを追加

            # Evaluation
            if len(self.sample) >= self.sample_window:
                self.sample_average = sum(self.sample)/len(self.sample)
                self.sample_max = max(self.sample)
                self.sample_min = min(self.sample)

                if abs(self.sample_max - self.sample_average) < self.th_w and abs(self.sample_min - self.sample_average) < self.th_w:
                    self.father_w = self.sample_average
                    self.sample.clear()
                    self.sample_average = 0.0
                    self.th_b = self.father_w + 1 #1kg上を設定する。
                    eval = True

        else:
            self.sample.clear()

        return eval

    def _proc_stage_B_evaluate_stage(self, weight):
        eval = False

        print("<<<<<<< | | | -> [service] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        # Sample Management
        # if len(self.sample) < self.sample_window: #3こ超えたら
        # if len(self.sample) < 5: #3こ超えたら
        #     self.sample.append(weight) # リストの後ろにサンプルを追加
        # # elif len(self.sample) >= self.sample_window:
        # elif len(self.sample) >= 5:
        #     self.sample.pop(0)  # リストの頭１つを削除
        #     self.sample.append(weight) # リストの後ろにサンプルを追加

        if 1.0 < weight :
            if len(self.sample) < 5:
                self.sample.append(weight) # リストの後ろにサンプルを追加
            elif len(self.sample) >= 5:
                self.sample.pop(0)  # リストの頭１つを削除
                self.sample.append(weight) # リストの後ろにサンプルを追加

            # Evaluation
            if len(self.sample) >= 5:
                self.sample_average = sum(self.sample)/len(self.sample)

            if weight < self.th_b_before:
                self.isFatherLeft = True
            if self.isFatherLeft:
                print("==== Father Left ====")
            if self.isFatherLeft and self.sample_average > self.th_b:
                self.sample.clear()
                self.sample_average = 0.0
                eval = True

        else:
            self.sample.clear()
    
        return eval

    def _proc_stage_C_evaluate_stage(self, weight):
        eval = False

        print("| | | | -> [service] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        if 1.0 < weight :

            # Sample Management // 関数として切り離すべき
            if len(self.sample) < self.sample_window: #3こ超えたら
                self.sample.append(weight) # リストの後ろにサンプルを追加
            elif len(self.sample) >= self.sample_window:
                self.sample.pop(0)  # リストの頭１つを削除
                self.sample.append(weight) # リストの後ろにサンプルを追加

            # Evaluation // 関数として切り離すべき
            if len(self.sample) >= self.sample_window:
                self.sample_average = sum(self.sample)/len(self.sample)
                self.sample_max = max(self.sample)
                self.sample_min = min(self.sample)

                if abs(self.sample_max - self.sample_average) < self.th_w and abs(self.sample_min - self.sample_average) < self.th_w:
                    self.father_child_w = self.sample_average
                    self.sample.clear()
                    self.child_w = self.father_child_w - self.father_w
                    eval = True

        else:
            self.sample.clear()

        return eval

    def _proc_stage_OVER_evaluate_stage(self, weight):
        eval = False
        # if weight < self.th_c:
        #     eval = True
        if self.isWeightEstimationEnd is True and (self.isRecordSuccess or self.isRecordEnded) is True:
            self._proc_push_to_database() # 例外処理を
            print("=======================================")
            print("Ending Program. |Result| Father Weight:" + str(self.father_w) +" Child Weight:" + str(self.child_w) + " Record Successed." )
            print("=======================================")
            self.counter += 1 #動作記録用

            self.isMeasureEnd = True
            eval = True
        return eval

    def controller(self, weight):
        print('[Controller] Stage Code:'+ str(self.CURRENT_STAGE) )# + 'Stage A Status Code A:'+ str(self.STAGE_A) + ' B:' + str(self.STAGE_B) + ' C:' + str(self.STAGE_C))
        if self.CURRENT_STAGE is self.stage['START']:
            if self.measure_process_end:
                print("Process Ended")
                return
            if self.isRecording:
                print("=== Recording ===")
            elif self._proc_stage_START_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['A']

                self.isFatherLeft = False
                self.isRecordSuccess = False
                self.isMeasureEnd = False

                print('Movint to Stage A')
                self.beep_start_measureing_2A() # Aへ行く時の音声再生
        elif self.CURRENT_STAGE is self.stage['A']:
            if self._proc_stage_A_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['B']
                print('Movint to Stage B')
                #計測が終わったら移動する。
                self.beep_start_measureing_2B() # Bへ行く時の音声再生 
        elif self.CURRENT_STAGE is self.stage['B']:
            if self._proc_stage_B_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['C']

                self.filename = str(uuid.uuid1())
                t = threading.Thread(target=self.record_omoi)
                t.setDaemon(True)
                t.start()

                print('Movint to Stage C')
                self.isRecordEnded = False
                self.beep_start_measureing_2C() # Cへ行く時の音声再生
        elif self.CURRENT_STAGE is self.stage['C']:
            if self._proc_stage_C_evaluate_stage(weight):
                self.CURRENT_STAGE = self.stage['OVER']
                #計測が終わったら移動する。

                self.isWeightEstimationEnd = True

                print('Movint to Stage OVER')
                self.beep_start_measureing_OVER()
        elif self.CURRENT_STAGE is self.stage['OVER']:
            if self._proc_stage_OVER_evaluate_stage(weight) and self.isRecordEnded:
                self.CURRENT_STAGE = self.stage['START']
                self.isMeasureProcessEnded = True
                self.measure_process_end = True
                print(" <<< Getting Back to the START Stage.")

        else:
            print('[WARNINNG] Controller is something wrong.')
        if self.isRecording:
            print("=== Recording ===")

    def _event_weight_simulation_event(self, ):
        weight = self.ws.get_current_w()
        print("| | -> [service] sim weight: " + str(weight))
        # if self.time > 0 and self.isView:
        #     self.view.update(weight,self.time)
        self.controller(weight)

    def _event_weight_event(self, ):
        weight = self.weighter.get_current_w()
        # weight =  10.0#self.ws.get_current_w()
        print("| | -> [service] weight: " + str(weight))
        # if self.time > 0 and self.isView:
        #     self.view.update(weight,self.time)
        self.controller(weight)

    def time_update(self, ):
        self.time += 1

    def update_measurement(self, ):
        print("============================================================")
        print('| Service update' + ' -> Current stage :'+ str(self.CURRENT_STAGE))

        if self.isSimulation:
            self._event_weight_simulation_event()
        else:
            self._event_weight_event()
        self.time_update()

    def start_real(self, ):
        print("||||||| Start Real. ||||||")
        try:
            while not self.stop_event_measure.is_set():
                for func in self.funcs_real:
                    t = threading.Thread(target=func)
                    t.setDaemon(True)
                    t.start()
                    self.threads.append(t)
                    for t in self.threads:
                        t.join()
                # if self.isMeasureEnd:
                #     self.isMeasureProcessEnded = True
                #     self.time = 0
                #     print(">>>>>>>>>>>>>>>>>.................<<<<<<<<<<<<<<<<<")
                #     print(">>>>>>>>>>>>>>> [!] Measure Stopping <<<<<<<<<<<<<<<")
                #     print(">>>>>>>>>>>>>>>>>.................<<<<<<<<<<<<<<<<<")
                #     self._proc_stop_measure()

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

    def start_simulator(self, ):
        try:
            #ここにforで、テスト回数を増やす
            while True:
                for func in self.funcs:
                    t = threading.Thread(target=func)
                    t.setDaemon(True)
                    t.start()
                    self.threads.append(t)
                    for t in self.threads:
                        t.join()
                if self.isMeasureEnd:
                    self.isMeasureProcessEnded = True
                    break

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

    def _proc_start_measure(self,):
        """計測・録音スレッド開始"""
        if self.isSimulation:
            self.start_simulator()
        else:
            self.start_real()

    def _proc_stop_measure(self,):
        """スレッドを停止させる"""
        self.stop_event_measure.set()
        self.th_measure.join() #スレッドが停止するのを待つ
        self.stop_event_measure.clear()

    def _proc_stop_wiiboard(self, ):
        """スレッドを停止させる"""
        print("WIIBOARD スレッド停止 !")
        stop_event_wiiboard.set()
        # self.th_weighter.join() #スレッドが停止するのを待つ
        stop_event_wiiboard.clear()

    # ============================================
    def _c_proc_get_near_weight_filename_from_local_database(self, target_weight):
        # con = MySQLdb.connect(
        #     user='omoinoomoi',
        #     passwd='omoinoomoi_Password_1',
        #     host='127.0.0.1',
        #     db='omoi_database'
        # )
        con = MySQLdb.connect(
            user='omoinoomosa',
            passwd='omoinoomosa',
            host='10.10.201.151',
            db='omoinoomosa'
        )
        cur = con.cursor()

        # sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
        cur.execute('select child_weight, record_name from master order by abs(`child_weight` - %s) limit 1', [target_weight])

        # rows = cur.fetchall()
        # for row in rows:
        #     print(row)
        # result = cur.fetchall()
        result = cur.fetchone()
        print("RESULT[0] is:"+str(result[0]))
        print("RESULT[1] is:"+str(result[1]))

        # con.commit()

        cur.close
        con.close

        # if abs(target_weight - result['child_weight'])  < 1:
        #     return result['record_name']
        # else:
        #     return None

        if len(result) is not 0:
            return result[1]
        else:
            return None

    def _proc_is_near_weight_filename_from_database(self, target_weight):
        print('Gettring from database')
        print("||| DATA ||| >>> target weight: " + str(target_weight))
        # we should use uuid. https://ja.wikipedia.org/wiki/UUID  https://www.python.ambitious-engineer.com/archives/1436
        # 下の関数は、通常呼びます。
        filename = self._c_proc_get_near_weight_filename_from_local_database(target_weight)
        if filename is None:
            return False
        else:
            self.target_play_file_name = filename
            return True

    def _proc_stage_MEASURE_IN_PLAY_evaluate_stage(self, weight):
        eval = False

        print("| | | -> [service:PLAY] sample info: " + str(self.sample) + " av:" + str(self.sample_average)+ " max:" + str(self.sample_max)+ " min:" + str(self.sample_min))

        if 1.0 < weight :

            # Sample Management
            if len(self.sample) < self.sample_window: #3こ超えたら
                self.sample.append(weight) # リストの後ろにサンプルを追加
            elif len(self.sample) >= self.sample_window:
                self.sample.pop(0)  # リストの頭１つを削除
                self.sample.append(weight) # リストの後ろにサンプルを追加

            # Evaluation
            if len(self.sample) >= self.sample_window:
                self.sample_average = sum(self.sample)/len(self.sample)
                self.sample_max = max(self.sample)
                self.sample_min = min(self.sample)

                if abs(self.sample_max - self.sample_average) < self.th_w and abs(self.sample_min - self.sample_average) < self.th_w:
                    self.target_real_object_w = self.sample_average
                    self.sample.clear()
                    self.sample_average = 0.0

                    #データベースに接続して、全部の体重持ってきて、近い体重があるか,
                    #あればファイル名を取得して、Trueを返す。ない限りここが回り続ける。
                    if self._proc_is_near_weight_filename_from_database(self.target_real_object_w):
                        print("target filename is :"+str(self.target_play_file_name))
                        eval = True
                    else:
                        print("Non near child weight and target file is found.")
        else:
            self.sample.clear()

        return eval
    
    def _proc_stage_PLAY_stage(self, filename):
        eval = False
        return self.play_sound_of_file(filename)

    def play_controller(self, weight):
        print('[Controller:PLAY] Stage Code:'+ str(self.PLAY_CURRENT_STAGE) )# + 'Stage A Status Code A:'+ str(self.STAGE_A) + ' B:' + str(self.STAGE_B) + ' C:' + str(self.STAGE_C))
        if self.PLAY_CURRENT_STAGE is self.play_stage['MEASURE_IN_PLAY']:
            if self.play_process_end:
                return

            if self._proc_stage_MEASURE_IN_PLAY_evaluate_stage(weight):
                self.PLAY_CURRENT_STAGE = self.play_stage['PLAY']
                print('Start Play')
                self.beep_start_measureing()
                time.sleep(2)
        elif self.PLAY_CURRENT_STAGE is self.play_stage['PLAY']:
            #if self._proc_stage_PLAY_stage(self.target_):
            self._proc_stage_PLAY_stage(self.target_play_file_name)
            self.PLAY_CURRENT_STAGE = self.play_stage['MEASURE_IN_PLAY']
            print('Stop Play')
            self.beep_play_end() # 音声再生 
            self.play_process_end = True
            return
        else:
            print('[WARNINNG] Controller is something wrong.')

    def _event_play_event(self, ):
        weight = self.weighter.get_current_w()
        print("| | -> [service:PLAY] weight: " + str(weight))
        self.play_controller(weight)

    def update_play(self,):
        print("============================================================")
        print("| Play update ")
        self._event_play_event()
    
    def start_player(self,):
        try:
            print("ln")
            while not self.stop_event_play.is_set():
                for func in self.funcs_p:
                    t = threading.Thread(target=func)
                    t.setDaemon(True)
                    t.start()
                    self.threads.append(t)
                    for t in self.threads:
                        t.join()

        except KeyboardInterrupt:
            print('\n Keyborad Interrupted! Aborting.')
            pass
            sys.exit()

    def _proc_start_play(self,):
        """再生スレッド開始"""
        print("play start")
        print("===== Play Proc is runnning ====")
        time.sleep(5)
        self.start_player()
        print("play end")

    def _proc_stop_play(self,):
        """スレッドを停止させる"""
        self.stop_event_play.set()
        self.th_play.join() #スレッドが停止するのを待つ
        self.stop_event_play.clear()

    def proc(self, ):
        # print("proc")
        # t = threading.Thread(target=test1)
        # t.setDaemon(True)
        # t.start()

        # try:
        #     t = threading.Thread(target=test1)
        #     t.setDaemon(True)
        #     t.start()
        #     # tt = threading.Thread(target=func)
        #     # tt.setDaemon(True)
        #     # tt.start()
        #     self.threads.append(t)
        #     # self.threads.append(tt)
        #     # for t in self.threads:
        #     #     t.join()
        # except KeyboardInterrupt:
        #     print('\n Keyborad Interrupted! Aborting.')
        #     pass
        #     sys.exit()
        pass

# def insert_test():
#     con = MySQLdb.connect(
#         user='omoinoomoi',
#         passwd='omoinoomoi_Password_1',
#         host='127.0.0.1',
#         db='omoi_database'
#     )
#     cur = con.cursor()

#     timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
#     child_weight = 10.0
#     parent_weight = 12.0
#     record_name = "test"

#     # sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values('"+ str(timestamp) +"',"+ str(child_weight) +","+ str(parent_weight) +",'"+ str(record_name) +"')"
#     sql = "insert into omoi_database.master (time, child_weight, parent_weight, record_name) values(%s,%s,%s,%s)"
#     r = cur.execute(sql, (str(timestamp),str(child_weight),str(parent_weight),record_name))
#     print(r)
#     con.commit()

#     cur.close
#     con.close

if __name__ == "__main__":
    # insert_test()

    # s = Service()
    # s.start_simulator()

    # 毎回電源が入ったらこれ。
    s = Service(False)
    # # s.weight_thread()
    # # s.gpoiTest()
    s.start_switch_watch()

    # Record dest
    # s = Service()
    # s.filename = "1903008_04_test"
    # s.record_omoi()

    # 体重計テスト
    # w = Weighter()
    # w.main()

    # ==================================

    # ws = WeightSimulator()
    # ws.start()
    # ws.simulation_test()

    # v = View()
    # v.start()
    # v.seflupdate()

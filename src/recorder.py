import pyaudio
import wave

from pydub import AudioSegment
import matplotlib.pyplot as plt
import pydub
from pydub.utils import db_to_float, ratio_to_db

class Recorder():
    def __init__(self,):
        pass
    
    def record(self, filename):
        result = self.main(filename)
        print("DONE : " + str(result))

    def main(self, filename):
        #form_1 = pyaudio.paInt16
        form_1 = pyaudio.paInt32
        chans = 2
        #samp_rate = 44100
        samp_rate = 48000
        #chunk = 4096
        chunk = 1024
        record_secs = 10 #int(input('1. レコード時間を秒数で入力してください。'))
        dev_index = 0
        wave_output_filename_body = filename #str(uuid.uuid1())#""#input('2. 保存ファイル名(.wavは除く)を英数で入力してください。')
        wave_output_filename = wave_output_filename_body + '.wav'

        audio = pyaudio.PyAudio()

        #device_list = []
        mic_available_device_list = []
        print("==showing list==")
        for ii in range(audio.get_device_count()):
            print(audio.get_device_info_by_index(ii).get('name'))
            print(audio.get_device_info_by_index(ii))
            #device_list.append(audio.get_device_info_by_index(ii).get('name'))
            print(" >> "+str(audio.get_device_info_by_index(ii).get('maxInputChannels')))
            mic_available_device_list.append(audio.get_device_info_by_index(ii).get('maxInputChannels'))

        device_count = 0
        for madl in mic_available_device_list:
            if madl is 1 or madl is 2:
                dev_index = device_count
                print("mic device found")
                break
            else:
                device_count = device_count + 1

        print("dev_index: " + str(dev_index))
        print("mic dev list: " + str(mic_available_device_list))
        stream = audio.open(format = form_1, rate = samp_rate, channels = chans, input_device_index = dev_index, input = True, frames_per_buffer = chunk)
        print("recording")
        frames = []

        for ii in range(0, int((samp_rate/chunk)*record_secs)):
            data = stream.read(chunk)
            frames.append(data)

        print("finishied recording")

        stream.stop_stream()
        stream.close()
        audio.terminate()

        wavefile = wave.open("../data/" + wave_output_filename, 'wb')
        wavefile.setnchannels(chans)
        wavefile.setsampwidth(audio.get_sample_size(form_1))
        wavefile.setframerate(samp_rate)
        wavefile.writeframes(b''.join(frames))
        wavefile.close()

        self.process_louder(filename)

        result = True
        return result

    def process_louder(self, filename):
        ratio = 7

        filename_wav = filename+".wav"

        #音声ファイル読み込み
        sound = AudioSegment.from_file("../data/"+filename_wav,"wav")
        print("sound length:" + str(sound.duration_seconds))

        #最初の0.5秒はカット
        cut_sound = sound[0.5*1000:]

        # ピーク確認
        print("f loudness:"+str(cut_sound.dBFS))

        # === Show
        # samples = cut_sound.get_array_of_samples()
        # plt.plot(samples)
        # plt.show()

        loud_sound = cut_sound + ratio_to_db(ratio)
        loud_sound.export("../data/"+filename_wav, format="wav")

        # === Show
        # samples_loud = loud_sound.get_array_of_samples()
        # plt.plot(samples_loud)
        # plt.show()

        # ピーク確認
        print("ff loudness:"+str(loud_sound.dBFS))

        #ここで、0dBFSを超えてないか確認しながら下げたりあげたりしたい。

        #音量増倍値を確認
        result_ratio = loud_sound.rms / cut_sound.rms
        print("rms:"+str(result_ratio))

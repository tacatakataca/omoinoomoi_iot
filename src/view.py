import math, random
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
import pandas as pd
import time, threading
import sys

class View(object):
    def __init__(self, *args, **kwargs):
        self.start()
    
    def start(self, ):
        self.times = [0 for i in range(100)]
        self.xs = [0 for i in range(100)]

        self.time = 0
        self.x = 0

        # initialize matplotlib
        plt.ion()
        plt.figure()
        self.li, = plt.plot(self.times, self.xs)

        plt.ylim(-1,100)
        plt.xlabel("time")
        plt.ylabel("weight")
        plt.title("weight real time plot")

        plt.xlim(min(self.times), max(self.times))
        plt.draw()
        plt.pause(0.01)

    def update(self, _x, _time):
        self.x = _x
        self.time = _time

        self.times.append(self.time)
        self.times.pop(0)
        self.xs.append(self.x)
        self.xs.pop(0)

        self.li.set_xdata(self.times)
        self.li.set_ydata(self.xs)

        plt.xlim(min(self.times), max(self.times))
        plt.draw()
        # plt.pause(0.01)

    def seflupdate(self, ):
        count = 0
        while True:
            self.update(math.sin(count), count)
            plt.pause(0.01)
            count += 1

import Adafruit_BluefruitLE

def main():
    provider = Adafruit_BluefruitLE.get_provider()
    provider.initialize()
    # provider.run_mainloop_with(main)
    adapter = provider.get_default_adapter()
    adapter.power_on()

    adapter.start_scan()
    device = provider.find_device()
    adapter.stop_scan()

    print(device.name)

if __name__ == "__main__":
    main()    